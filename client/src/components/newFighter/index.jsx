import TextField from '@material-ui/core/TextField';
import { createFighter, updateFighter } from "../../services/domainRequest/fightersRequest";
import React, { useEffect, useState } from "react";
import { Box, Button } from "@material-ui/core";
import './newFighter.css';
import { makeStyles } from "@material-ui/core/styles";
import Snackbars from "../snackbars";

const useStyles = makeStyles((theme) => ({
    root: {
        '&': {
            flexWrap: 'wrap',
            justifyContent: 'space-around'
        }
    },
    width: {
        width: '90%'
    },
    margin: {
        '&': {
            margin: '3% 1% 0'
        },
    },
    but: {
        '&': {
            width: '130px',
            margin: '1rem;'
        },
    }
}));

export default function NewFighter({ onCreated, editFighter, updateFighters}) {
    const classes = useStyles();
    const [name, setName] = useState(editFighter?.name || '');
    const [health, setHealth] = useState(editFighter?.health || 100);
    const [power, setPower] = useState(editFighter?.power || 50);
    const [defense, setDefense] = useState(editFighter?.defense || 5);
    const [errMsgObj, setErrMsg] = useState({});
    const [openSnackbar, setOpenSnackbar] = React.useState(false);

    useEffect( () => {
        setName(editFighter?.name || '');
        setHealth(editFighter?.health || 100);
        setPower(editFighter?.power || 50);
        setDefense(editFighter?.defense || 5);
        setErrMsg({})
    }, [editFighter])

    const onNameChange = (event) => {
        setName(event.target.value);
    }

    const onPowerChange = (event) => {
        const value = event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
        setPower(value);
    }

    const onHealthChange = (event) => {
        const value = event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
        setHealth(value);
    }

    const onDefenseChange = (event) => {
        const value = event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
        setDefense(value);
    }

    const onSubmit = async () => {
        const data = editFighter
          ? await updateFighter(editFighter.id, { health, name, power, defense })
          : await createFighter({ health, name, power, defense });
        setErrMsg('');
        // updateFighters = true;

        if(data.error) {
            setErrMsg(JSON.parse(data.message || 'Error'));
        }

        if(data && !data.error) {
            editFighter? editFighter=null : onCreated(data);
            setName('');
            setHealth(100);
            setPower(50);
            setDefense(5);
        }
        return data;
    }

    return (
        <div id="new-fighter">
            <div>{editFighter? 'Edit' : 'New'} Fighter</div>
            <TextField
              error={!!errMsgObj['name']}
              helperText={errMsgObj['name'] ? errMsgObj['name'] : ''}
              onChange={onNameChange}
              variant="outlined"
              required
              value={name}
              className={'newNameFighter'}
              id="name"
              label="Name"
              placeholder="Name"/>
            <Box display="flex" className={classes.margin} id="newFighterSpec">
                <TextField
                  error={!!errMsgObj['health']}
                  helperText={errMsgObj['health'] ? errMsgObj['health'] : ''}
                  onChange={onHealthChange}
                  variant="outlined"
                  id="health"
                  label="Health"
                  placeholder="80 < health < 120"
                  value={health}
                  className={classes.margin}
                  type="number" />
                <TextField
                  error={!!errMsgObj['power']}
                  helperText={errMsgObj['power'] ? errMsgObj['power'] : ''}
                  onChange={onPowerChange}
                  variant="outlined"
                  required
                  id="power"
                  label="Power"
                  value={power}
                  placeholder="1 < power < 100"
                  className={classes.margin}
                  type="number" />
                <TextField
                  error={!!errMsgObj['defense']}
                  helperText={errMsgObj['defense'] ? errMsgObj['defense'] : ''}
                  onChange={onDefenseChange}
                  required
                  variant="outlined"
                  id="defense"
                  label="Defense"
                  value={defense}
                  placeholder="1 < defense < 10"
                  className={classes.margin}
                  type="number" />
            </Box>
            <Snackbars btnClass={classes.but} editFighter={editFighter} bntClick={onSubmit}/>
        </div>
    );
};
