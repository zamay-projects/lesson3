const { check, validationResult } = require('express-validator');

const createFighterValid = async (req, res, next) => {
    const trim = { ignore_whitespace: true };
    await check('name', 'Enter name')
      .notEmpty(trim)
      .run(req);
    await check('health', "Specify the health from 80 to 120")
      .isInt({ min: 80, max: 120 })
      .run(req);
    await check('power', "Specify the strength from 1 to 100")
      .isInt({ min: 1, max: 100 })
      .run(req);
    await check('defense', "Specify the level of protection from 1 to 10")
      .isInt({ min: 1, max: 10 })
      .run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      let resMsgObj = {};
      errors.errors.forEach(item => {
        resMsgObj[item.param] = item.msg;
      });
      return res.status(400)
        .json({
          error: true,
          message: JSON.stringify(resMsgObj)
        });
    }

    next();
}

const updateFighterValid = async (req, res, next) => {
    const trim = { ignore_whitespace: true };
    for (let key in req.body) {
      if (key === 'name') {
        await check('name', 'Enter name')
          .notEmpty(trim)
          .run(req);
        continue;
      } else if (key === 'power') {
        await check('power', "Specify the strength from 1 to 100")
          .isInt({ min: 1, max: 100 })
          .run(req);
        continue;
      }else if (key === 'health') {
        await check('health', "Specify the health from 80 to 120")
          .isInt({  min: 80, max: 120  })
          .run(req);
        continue;
      } else if (key === 'defense') {
        await check('defense', "Specify the level of protection from 1 to 10")
          .isInt({ min: 1, max: 10 })
          .run(req);
        continue;
      } else {
        return res.status(400)
          .json({
            error: true,
            message: 'Forbidden field'
          });
      }
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      let resMsg = '';
      errors.errors.forEach(msg => resMsg += `${msg.msg}; `);
      return res.status(400)
        .json({
          error: true,
          message: resMsg
        });
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
