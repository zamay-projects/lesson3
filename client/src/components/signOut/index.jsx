import { unsetLoginSession } from "../../services/authService";
import React from 'react';
import './signOut.css';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

export default function SignOut({ isSignedIn, onSignOut}) {
    const signOut = () => {
        unsetLoginSession();
        onSignOut();
    }

    if(isSignedIn) {
        return (
            <div onClick={signOut} id="sign-out">
                <ExitToAppIcon />
            </div>
        )
    }

    return null;
}
