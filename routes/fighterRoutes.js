const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', async (req, res, next) => {
  try {
    res.data = await FighterService.getAll();
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', async (req, res, next) => {
  try {
    res.data = await FighterService.getOne(req.params.id);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', createFighterValid, async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = await FighterService.create(req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', updateFighterValid, async (req, res, next) => {
  if(res.err){
    next();
    return;
  }

  try {
    res.data = await FighterService.update(req.params.id, req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', async (req, res, next) => {
  try {
    res.data = await FighterService.delete(req.params.id);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

module.exports = router;
