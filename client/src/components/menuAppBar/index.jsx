import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SignOut from "../signOut";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  header: {
    '& > div': {
      margin: '0 10px'
    }
  },
  user: {
    display: "flex",
    '& > div': {
      margin: '0 10px'
    }
  },
  userName: {
    lineHeight: '33px'
  },
  cursorBtn: {
    cursor: 'pointer'
  }
}));

export default function MenuAppBar({ isSignedIn, onSignOut, userInfo}) {
  const classes = useStyles();

  return (
    <div className={classes.grow}>
      <AppBar position="static" >
        <Toolbar className={classes.header}>
          <div className={classes.user}>
            <div className={'user-icon'}>
              <AccountCircleIcon fontSize="large" className={classes.cursorBtn}/>
            </div>
            <div className={classes.userName}>
              {userInfo.firstName + ' ' + userInfo.lastName}
            </div>
          </div>

          <div className={classes.grow}/>

          <div className={classes.cursorBtn}>
            <SignOut isSignedIn={isSignedIn} onSignOut={onSignOut} />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
