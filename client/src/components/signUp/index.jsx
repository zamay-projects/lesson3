import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';

import { createUser } from '../../services/domainRequest/userRequest';
import { setLoginSession } from '../../services/authService';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            width: '100% !important',
            margin: '10px 0'
        },
    },
    but: {
        '&': {
            height: '48px'
        }
    }
}));

export default function SignUp({ setIsLoggedIn }) {
    const classes = useStyles();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [errMsgObj, setErrMsg] = useState({});

    const clearErrorOnInput = (inputName) => {
        if(errMsgObj[inputName]) {
            const item = Object.assign({}, errMsgObj)
            delete item[inputName];
            setErrMsg(item)
        }
    }

    const onEmailChange = (event) => {
        clearErrorOnInput('email');
        setEmail(event.target.value);
    }

    const onPasswordChange = (event) => {
        clearErrorOnInput('password');
        setPassword(event.target.value);
    }

    const onFirstNameChange = (event) => {
        clearErrorOnInput('firstName');
        setFirstName(event.target.value);
    }

    const onLastNameChange = (event) => {
        clearErrorOnInput('lastName');
        setLastName(event.target.value);
    }

    const onPhoneNumberChange = (event) => {
        clearErrorOnInput('phoneNumber');
        setPhoneNumber(event.target.value);
    }

    const onSubmit = async () => {
        const data = await createUser({ email, password, firstName, lastName, phoneNumber });
        if(data.error) {
            setErrMsg(JSON.parse(data.message || 'Error'));
        }

        if(data && !data.error) {
            setLoginSession(data);
            setIsLoggedIn(true);
            setErrMsg({});
        }
    }

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <TextField key="first-name"
                       error={!!errMsgObj['firstName']}
                       helperText={errMsgObj['firstName'] ? errMsgObj['firstName'] : ''}
                       onChange={onFirstNameChange}
                       required
                       id="first-name"
                       variant="outlined"
                       label="First Name"
                       placeholder="First Name"/>
            <TextField key="last-name"
                       error={!!errMsgObj['lastName']}
                       helperText={errMsgObj['lastName'] ? errMsgObj['lastName'] : ''}
                       onChange={onLastNameChange}
                       required id="last-name"
                       variant="outlined"
                       label="Last Name"
                       placeholder="Last Name"/>
            <TextField key="email"
                       error={!!errMsgObj['email']}
                       helperText={errMsgObj['email'] ? errMsgObj['email'] : ''}
                       onChange={onEmailChange}
                       required id="email"
                       variant="outlined"
                       label="Email"
                       placeholder="Enter correct e-mail"/>
            <TextField key="phoneNumber"
                       error={!!errMsgObj['phoneNumber']}
                       helperText={errMsgObj['phoneNumber'] ? errMsgObj['phoneNumber'] : ''}
                       onChange={onPhoneNumberChange}
                       required id="phoneNumber"
                       variant="outlined"
                       label="Phone Number"
                       placeholder="+380XXXXXXX"/>
            <TextField key="password"
                       error={!!errMsgObj['password']}
                       helperText={errMsgObj['password'] ? errMsgObj['password'] : ''}
                       onChange={onPasswordChange}
                       required id="password"
                       variant="outlined"
                       label="Password"
                       placeholder="Minimum length 3"
                       type="password"/>
            <Button className={classes.but} onClick={onSubmit} variant="contained" color="primary">Sign Up</Button>
        </form>
    )
}
