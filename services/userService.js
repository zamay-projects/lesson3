const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        const items = UserRepository.getAll();
        if (!items) {
            throw Error('No users found');
        }
        return items;
    }

    getOne(id) {
        const item = UserRepository.getOne({ id });
        if(!item){
            throw Error('User doesn\'t exist.');
        }
        return item;
    }

    create(userData) {
        const isEmailExist = UserRepository.getOne({
            email: userData.email
        });
        const isPhoneExist = UserRepository.getOne({
            phoneNumber: userData.phoneNumber
        });
        if (!isEmailExist && !isPhoneExist) {
            const item = UserRepository.create(userData);
            if (!item) {
                throw Error('Server error');
            }
            delete item.id;
            return item;
        } else if (isEmailExist) {
            throw Error( JSON.stringify({email: 'Email already exists'}));
        } else {
            throw Error(JSON.stringify({phoneNumber: 'PhoneNumber already exists'} ));
        }
    }

    update(id, user) {
        const item = UserRepository.update(id, user);
        if (!updatedUser) {
            throw Error('Can not update user');
        }
        delete item.id;
        return item;
    }

    delete(id) {
        const item = UserRepository.delete(id);
        if (!item.length) {
            throw Error('No such user');
        }
        return item;
    }

    login(search) {
        const { email, password } = { ...search };

        if (!email) throw Error( JSON.stringify({email: 'Enter your email'}));
        if (!password) throw Error( JSON.stringify({password: 'Enter your password'}));

        const isEmailExist = UserRepository.getOne({
            email: email
        });

        const item =  UserRepository.getOne(search)

        if (isEmailExist && item) {
            delete item.id;
            return item;
        } else if (isEmailExist) {
            throw Error( JSON.stringify({email: 'Incorrect password'}));
        } else {
            throw Error( JSON.stringify({email: 'User not found'}));
        }
    }
}

module.exports = new UserService();
