const { fighter } = require("../models/fighter");
const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAll() {
    const items = FighterRepository.getAll();
    if (!items) {
      throw Error('No fighters found');
    }
    return items;
  }

  getOne(id) {
    const item = FighterRepository.getOne({ id });
    if(!item){
      throw Error('Fighter doesn\'t exist.');
    }
    return item;
  }

  create(fighterData) {
    const ifFighterExist = FighterRepository.getOne({
      name: fighterData.name
    });
    if(!ifFighterExist) {
      const newFighter = { ...fighter, ...fighterData };
      const item = FighterRepository.create(newFighter);
      if (!item) {
        throw Error('Server error');
      }
      // delete item.id;
      return item;
    } else {
      throw Error('Name already exists');
    }
  }

  update(id, fighter) {
    const item = FighterRepository.update(id, fighter);
    if (!item) {
      throw Error('Fighter not found');
    }

    // delete item.id;
    return item;
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item.length) {
      throw Error('No such fighter');
    }
    return item;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      throw Error('No such fighter found');
    }
    delete item.id;
    return item;
  }
}

module.exports = new FighterService();
