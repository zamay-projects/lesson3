import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';

import './signIn.css';
import { login } from '../../services/domainRequest/auth';
import { setLoginSession } from '../../services/authService';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            width: '100% !important',
            margin: '10px 0'
        },
    },
    but: {
        '&': {
            height: '48px'
        }
    }
}));

export default function SignIn({ setIsLoggedIn }) {
    const classes = useStyles();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [errMsgObj, setErrMsg] = useState({});

    const onEmailChange = (event) => {
        if(event.target.value.match(/[a-zA-Z0-9]+(@gmail.com$)/)) {
            setEmail(event.target.value);
            setErrMsg({});
        } else {
            setErrMsg({email: 'Enter correct gmail | ([a-zA-Z0-9]+(@gmail.com$)'});
        }
    }

    const onPasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const onSubmit = async () => {
        const data = await login({ email, password });

        if(data.error) {
            setErrMsg(JSON.parse(data.message));
        }

        if(data && !data.error) {
            setLoginSession(data);
            setIsLoggedIn(true);
            setErrMsg({});
        }
    }

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <TextField
              key="email"
              error={!!errMsgObj['email']}
              helperText={errMsgObj['email'] ? errMsgObj['email'] : ''}
              onChange={onEmailChange}
              required
              id="email"
              variant="outlined"
              label="Email"
              placeholder="Enter correct e-mail"/>
            <TextField
              key="password"
              error={!!errMsgObj['password']}
              helperText={errMsgObj['password'] ? errMsgObj['password'] : ''}
              onChange={onPasswordChange}
              required
              id="password"
              variant="outlined"
              label="Password"
              placeholder="Minimum length 3"
              type="password"/>
            <Button className={classes.but} onClick={onSubmit} variant="contained" color="primary">Sign In</Button>
        </form>
    )
}
