const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();
router.get('/', async (req, res, next) => {
  try {
    const items = await FightService.getAll();
    res.data = item;
  } catch (error) {
    res.err = error;
  } finally {
    next()
  }
}, responseMiddleware)

router.get('/:id', async (req, res, next) => {
  try {
    const item = await FightService.getOne(req.params.id);
    res.data = item;
  } catch (error) {
    res.err = error;
  } finally {
    next()
  }
}, responseMiddleware)

router.post('/', async (req, res, next) => {
  try {
    const item = await FightService.create(req.body);
    res.data = item
  } catch (error) {
    res.err = error
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', async (req, res, next) => {
  try {
    const item = await FightService.delete(req.params.id);
    res.data = item;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

module.exports = router;
