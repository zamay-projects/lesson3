import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { getLoginSession, isSignedIn } from '../../services/authService';
import Fight from '../fight';
import MenuAppBar from "../menuAppBar";

class StartScreen extends React.Component {

    state = {
        isSignedIn: false,
        user: {}
    };

    async componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
        const user = await getLoginSession();
        if (user && !user.error) {
            this.setState({user});
        }
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState({isSignedIn});
    }

    render() {
        const {isSignedIn, user} = this.state;
        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn}/>
        }

        return (
          <>
              <MenuAppBar userInfo={user} isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)}/>
              <Fight/>
          </>
        );
    }
}

export default StartScreen;
