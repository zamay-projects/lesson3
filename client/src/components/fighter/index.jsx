import React, { useState } from 'react';
import { FormControl, InputLabel, makeStyles, Select, Box } from '@material-ui/core';
import { MenuItem } from 'material-ui';
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: '5% auto',
        width: '96%',
        minWidth: 120,
        display: 'flex',
        justifyContent: 'space-evenly'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    margin: {
        '&': {
            margin: '3% 0 0',
            justifyContent: 'space-between'
        },
    },
}));

export default function Fighter({ fightersList, onFighterSelect, selectedFighter, numberFighter}) {
    const classes = useStyles();
    const [fighter, setFighter] = useState();

    const handleChange = (event) => {
        setFighter(event.target.value);
        onFighterSelect(event.target.value);
    };

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="simple-select">{numberFighter} Fighter</InputLabel>
                <Select
                    id="simple-select"
                    label={numberFighter + " Fighter"}
                    value={fighter}
                    onChange={handleChange}>
                    { fightersList.length > 0
                      ? fightersList.map((it, index) => {
                          return (
                            <MenuItem key={`${index}`} value={it}>{it.name}</MenuItem>
                          );
                        })
                      : <MenuItem>Create new fighter</MenuItem>
                    }
                </Select>
                {selectedFighter ?
                     <Box class="spec">
                         <TextField
                           variant="outlined"
                           InputProps={{
                               readOnly: true,
                           }}
                           label="Health"
                           value={selectedFighter.health}/>
                         <TextField
                           variant="outlined"
                           InputProps={{
                               readOnly: true,
                           }}
                           label="Power"
                           value={selectedFighter.power}/>
                         <TextField
                           variant="outlined"
                           InputProps={{
                               readOnly: true,
                           }}
                           label="Defense"
                           value={selectedFighter.defense}/>
                    </Box>
                    : null
                }
            </FormControl>
        </div>)
}
