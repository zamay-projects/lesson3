import React from 'react';
import { FormControl, InputLabel, makeStyles, Select } from '@material-ui/core';
import { MenuItem } from 'material-ui';

const useStyles = makeStyles((theme) => ({
  formControl: {
    background: 'rgba(255, 255, 255, 0.15)',
    '&:hover': {
      background: 'rgba(255, 255, 255, 0.25)',
    },
    '& > label': {
      lineHeight: '0'
    },
    borderRadius: '5%',
    minWidth: 130,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  padding: {
    '& > div': {
      padding: '8px',
    },
  }
}));

export default function MySelect({fightersList, onFighterSelect}){
  const classes = useStyles();

  const handleChange = (event) => {
    onFighterSelect(event.target.value);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel htmlFor="outlined-age-native-simple">Edit Fighter</InputLabel>
      <Select
        onChange={handleChange}
        className={classes.padding}
        label="Edit Fighter">
        { fightersList.length > 0
          ? fightersList.map((it, index) => {
            return (
              <MenuItem key={`${index}`} value={it}>{it.name}
              </MenuItem>
            );
          })
          : <MenuItem>Create new fighter</MenuItem>
        }
      </Select>
    </FormControl>
  )
}
