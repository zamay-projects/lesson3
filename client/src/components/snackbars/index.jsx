import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function Snackbars({btnClass, editFighter, bntClick}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        const isError = bntClick();
        isError.then((data) => {
            if(!data.error) {
                setOpen(true);
            }
        })
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
      <div className={classes.root}>
          <Button className={btnClass} variant="contained" color="primary" onClick={handleClick}>
              {editFighter? 'Edit' : 'Create'}
          </Button>
          <Snackbar open={open} autoHideDuration={3000}
                    onClose={handleClose}
                    anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
              <Alert onClose={handleClose} severity="success">
                  {editFighter? 'Edit' : 'Create'} fighter success!
              </Alert>
          </Snackbar>
      </div>
    );
}
