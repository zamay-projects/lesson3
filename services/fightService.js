const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  // TODO: посмотреть все бои / посмотреть 1 бой / записать бой / удалить бой
  getAll() {
    const logs = FightRepository.getAll();
    if(!logs.length) {
      throw Error('no logs')
    }
    return logs;
  }

  getOne(fightData) {
    try {
      let fight = FightRepository.getOne(fightData);
      if (!fight) {
        throw Error('Fight not found');
      }
      return fight;
    } catch (error) {
      throw Error('Fight not found');
    }
  }

  create(data) {
    const log = FightRepository.create(data);
    if(!log) {
      throw Error('Error writting')
    }
    return log;
  }

  delete(id) {
    const item = FightRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FightersService();
