import React from 'react';
import { getFighters } from '../../services/domainRequest/fightersRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import './fight.css'
import MySelect from "../mySelect";

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fighter: null,
        showComponent: true,
        date: new Date(),
        updateFighters: false, //not working :(
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.updateFighters !== prevState.updateFighters) {
            this.setState( {updateFighters: false});

            const fighters = await getFighters();
            if(fighters && !fighters.error) {
                this.setState({ fighters });
            }
        }
    }

    onFightStart = () => {
        
    }

    onCreate = (fighter) => {
        !Object.keys(fighter).length
          ? this.onFighterSelect(null)
          : this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.name !== fighter2.name);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.name !== fighter1.name);
    }

    onFighterSelect = (fighter) => {
        this.setState({fighter});
        this.setState({updateFighters: true})
    }

    createNewFighter = () => {
        this.setState({fighter: null});
    };

    render() {
        const { fighters, fighter1, fighter2, fighter, updateFighters } = this.state;
        const [ f1, f2 ] = ['First', 'Second'];
        return (
            <div id="wrapper">
                <div className={'edit-button'}>
                    <Button onClick={this.createNewFighter} variant="contained">Create</Button>
                    <MySelect fightersList={fighters} onFighterSelect={this.onFighterSelect}/>
                </div>
                { this.state.showComponent &&
                  <NewFighter onCreated={this.onCreate} editFighter={fighter} updateFighters={updateFighters} /> }

                <div id="figh-wrapper">
                    <Fighter selectedFighter={fighter1}
                             numberFighter={f1}
                             onFighterSelect={this.onFighter1Select}
                             fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart}
                                disabled={!fighter1 || !fighter2}
                                style={{width: "130px"}}
                                variant="contained" color="primary">Start Fight
                        </Button>
                    </div>
                    <Fighter selectedFighter={fighter2}
                             numberFighter={f2}
                             onFighterSelect={this.onFighter2Select}
                             fightersList={this.getFighter2List() || []} />
                </div>
            </div>
        );
    }
}

export default Fight;
